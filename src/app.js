import '@tarojs/async-await';
import Taro, { Component } from '@tarojs/taro';
import { Provider } from '@tarojs/redux';

import Index from './pages/home/index';
import './app.scss';

import configStore from './store';

const store = configStore();

class App extends Component {

    config = {
        pages: [
            'pages/browse/index', //发现
            'pages/home/index', //主页
            'pages/shopping/index', //购物车
            'pages/me/index', //我的,
            'pages/buy/index', //物品详情、购买页面
            'pages/search/index' //搜索页面
        ],
        window: {
            backgroundTextStyle: 'light',
            navigationBarBackgroundColor: '#fff',
            navigationBarTitleText: 'WeChat',
            navigationBarTextStyle: 'black'
        },
        tabBar: {
            color: '#333',
            selectedColor: '#2aa515',
            backgroundColor: '#fff',
            borderStyle: '#ccc',
            list: [{
                pagePath: 'pages/home/index',
                text: '首页',
                iconPath: './images/tabBar/tabbar_home.png',
                selectedIconPath: './images/tabBar/tabbar_home_fill.png'
            }, {
                pagePath: 'pages/browse/index',
                text: '发现',
                iconPath: './images/tabBar/tabbar_search.png',
                selectedIconPath: './images/tabBar/tabbar_search_fill.png'
            }, {
                pagePath: 'pages/shopping/index',
                text: '购物车',
                iconPath: './images/tabBar/tabbar_shopping.png',
                selectedIconPath: './images/tabBar/tabbar_shopping_fill.png'
            }, {
                pagePath: 'pages/me/index',
                text: '我的',
                iconPath: './images/tabBar/tabbar_me.png',
                selectedIconPath: './images/tabBar/tabbar_me_fill.png'
            }]
        }
    };

    componentDidMount() {}

    componentDidShow() {}

    componentDidHide() {}

    componentCatchError() {}

    componentDidCatchError() {}

    render() {
        return ( <
            Provider store = { store } >
            <
            Index / >
            <
            /Provider>
        )
    }
}

Taro.render( < App / > , document.getElementById('app'))