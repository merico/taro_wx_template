import { Component } from '@tarojs/taro';
import { AtAvatar, AtGrid } from 'taro-ui'
import { Image } from '@tarojs/components';

import HaveReced from '../../images/goodsStatus/have-been-received.png';
import Refund from '../../images/goodsStatus/refund.png';
import ToBeDelivered from '../../images/goodsStatus/to-be-delivered.png';
import ToBeReced from '../../images/goodsStatus/to-be-received.png';

import './index.scss';

class Index extends Component {
    constructor(props) {
        super(props);
    }

    config = {
        navigationBarTitleText: '我的小屋'
    };

    render() {
        return (
            <View className='me-container'>
                <View className='me-info'>
                    <View className='me-avatar'>
                        <AtAvatar circle size='large' image='https://jdc.jd.com/img/200'></AtAvatar>
                    </View>
                    <View className='me-name'>
                        <Text>风吹过的夏夜</Text>
                    </View>
                    <View className='me-vip'>
                        <Text>会员</Text>
                    </View>
                </View>
                <View className='me-goods-status'>
                    <View className='goods-status-item'>
                        <View className='status-icon'>
                            <Image style='height:80%;width:50%' src={ToBeDelivered} />
                        </View>
                        <View className='status-desc'>
                            <Text>待发货</Text>
                        </View>
                    </View>
                    <View className='goods-status-item'>
                        <View className='status-icon'>
                            <Image style='height:80%;width:50%' src={ToBeReced} />
                        </View>
                        <View className='status-desc'>
                            <Text>待签收</Text>
                        </View>
                    </View>
                    <View className='goods-status-item'>
                        <View className='status-icon'>
                            <Image style='height:80%;width:50%' src={HaveReced} />
                        </View>
                        <View className='status-desc'>
                            <Text>已签收</Text>
                        </View>
                    </View>
                    <View className='goods-status-item'>
                        <View className='status-icon'>
                            <Image style='height:80%;width:50%' src={Refund} />
                        </View>
                        <View className='status-desc'>
                            <Text>退货/退款</Text>
                        </View>
                    </View>
                </View>
                <View className='me-setting'>
                    <AtGrid data={
                        [
                            {
                                image: 'https://img20.360buyimg.com/jdphoto/s72x72_jfs/t15151/308/1012305375/2300/536ee6ef/5a411466N040a074b.png',
                                value: '找折扣'
                            },
                            {
                                image: 'https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png',
                                value: '购物券'
                            },

                            {
                                image: 'https://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png',
                                value: '购物积分'
                            },
                            {
                                image: 'https://img30.360buyimg.com/jdphoto/s72x72_jfs/t5770/97/5184449507/2423/294d5f95/595c3b4dNbc6bc95d.png',
                                value: '收货地址'
                            },
                            {
                                image: 'https://img12.360buyimg.com/jdphoto/s72x72_jfs/t10660/330/203667368/1672/801735d7/59c85643N31e68303.png',
                                value: '意见反馈'
                            }
                        ]
                    } />
                </View>
            </View>
        )
    }
}

export default Index;
