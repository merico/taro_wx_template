import { Component } from '@tarojs/taro';
import { ScrollView } from '@tarojs/components';
import { AtButton, AtCurtain } from 'taro-ui'
import CustomRadio from '../../components/Radio';
import Goods from '../../components/Goods';

import './index.scss';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allSelected: false,
            showCurtain: false,
            totalCost: [],
            curtainSrc: '',
            MockData: [ //TODO: 购物车数据是从redux中拷贝出来的
                {
                    id: '111111',
                    name: '2018秋季新款宽松慵懒长袖网红毛衣文艺复古针织衫女装',
                    imageSrc: 'http://gd4.alicdn.com/imgextra/i4/34203113/TB2_QgfkHBmpuFjSZFuXXaG_XXa_!!34203113.jpg',
                    size: 'S',
                    price: 388,
                    number: 1,
                }, {
                    id: '222222',
                    name: '加绒卫衣连衣裙女2018秋冬新款休闲连帽中长款拼接宽松打底裙',
                    imageSrc: 'http://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i2/3476537169/O1CN01xwmMiu22pRsGEv7tU_!!3476537169.jpg_430x430q90.jpg',
                    size: 'M',
                    price: 468,
                    number: 1,
                }, {
                    id: '333333',
                    name: '鬼马系心机少女早秋款韩国复古英伦风漏肩小性感格纹连衣裙',
                    imageSrc: 'http://gd1.alicdn.com/imgextra/i3/824862565/O1CN011UooNCX3UswoEQJ_!!824862565.jpg',
                    size: 'S',
                    price: 154,
                    number: 1,
                }
            ]
        }
    }

    config = {
        navigationBarTitleText: '购物车'
    }

    handleAddOrRemoveGoods = (id, flag) => {
        let price, number;
        if (flag) {
            let totalCost = this.state.totalCost.slice();
            this.state.MockData.forEach(el => {
                if (el.id === id) {
                    price = el.price;
                    number = el.number;
                }
            })
            totalCost.push({ id, price, number });
            this.setState({
                totalCost: totalCost
            }, () => { this.setState({ allSelected: this.state.MockData.length === this.state.totalCost.length }) })
        } else {
            let idx, totalCostCopy = this.state.totalCost;
            totalCostCopy.forEach((el, index) => {
                if (el.id === id) {
                    idx = index;
                }
            })
            totalCostCopy.splice(idx, 1);
            this.setState({
                totalCost: totalCostCopy
            }, () => { this.setState({ allSelected: this.state.MockData.length === this.state.totalCost.length }) })
        }
    }

    handleAddOneOrMinusOne = (id, number) => {
        //修改MockData
        //TODO: 未来该数据会被保存在redux中。所以在修改完数值后会同步到redux中(同时删除每一条中的 selected 属性)
        this.state.MockData.forEach((el) => {
            if (el.id === id) {
                el.number = number;
            }
        })

        //修改totalCost
        this.state.totalCost.forEach((el, idx) => {
            if (el.id === id) {
                let totalCostCopy = this.state.totalCost;
                totalCostCopy[idx].number = number;
                this.setState({
                    totalCost: totalCostCopy
                });
            }
        })
    }

    handleShowCurtain = (imageSrc) => {
        this.setState({
            curtainSrc: imageSrc,
            showCurtain: true
        })
    }

    handleCloseCurtain = () => {
        this.setState({
            showCurtain: false
        })
    }

    handleSelectAllGoods = (index, isSelected) => {
        this.setState({
            allSelected: isSelected
        }, () => {
            let mockDataCopy = this.state.MockData.slice();
            let totalCostCopy = this.state.totalCost.slice();
            totalCostCopy.length = 0;//清空数组
            //修改每一条状态
            mockDataCopy.map(el => el['selected'] = this.state.allSelected);

            //如果是全选中，则把购物车数据全部放入totalCost中
            if (isSelected) {
                mockDataCopy.forEach(el => {
                    totalCostCopy.push({ id: el.id, number: el.number, price: el.price })
                })
            }
            this.setState({
                MockData: mockDataCopy,
                totalCost: totalCostCopy
            })
        })
    }

    onScrollToBottom = () => {
        console.log('load more');
    }

    render() {
        return (
            <View className='shopping-container'>
                <View className='shopping-header'>
                    <View className='shopping-header_select'>
                        <CustomRadio selected={this.state.allSelected} onChange={this.handleSelectAllGoods} />
                    </View>
                    <View className='shopping-header_total-count'>
                        合计: ¥<Text style='color:red'>{this.state.totalCost.length > 0 ? this.state.totalCost.reduce((prev, cur) => (prev + cur.price * cur.number), 0) : 0}</Text>
                    </View>
                    <View className='purcahse-btn'>
                        <AtButton type='secondary' size='small'>立即下单</AtButton>
                    </View>
                </View>
                <ScrollView
                    className='shopping-body' scrollY scrollWithAnimation
                    enableBackToTop lowerThreshold='20' onScrollToLower={this.onScrollToBottom}
                >
                    {
                        this.state.MockData.length > 0 ? (
                            this.state.MockData.map((el, idx) =>
                                <Goods
                                    key={idx}
                                    index={el.id}
                                    name={el.name}
                                    size={el.size}
                                    price={el.price}
                                    number={el.number}
                                    imageSrc={el.imageSrc}
                                    selected={el.selected}
                                    onChange={this.handleAddOrRemoveGoods}
                                    onHandleShowCurtain={this.handleShowCurtain}
                                    onHandleAddOneOrMinusOne={this.handleAddOneOrMinusOne}
                                />, this)
                        ) : (<Text>购物车空了</Text>)
                    }
                </ScrollView>
                <AtCurtain isOpened={this.state.showCurtain} onClose={this.handleCloseCurtain}>
                    <Image src={this.state.curtainSrc} />
                </AtCurtain>
            </View>
        )
    }
}

export default Index;
