import Taro, { Component } from '@tarojs/taro';
import { AtTabs, AtTabsPane } from 'taro-ui'
import SearchIcon from '../../images/browsePage/search.png';

import './index.scss';
class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
        }
    }

    config = {
        navigationBarTitleText: '发现宝贝'
    };

    handleClick(value) {
        this.setState({
            current: value
        })
    }

    handleNaviToSearch = () => {
        Taro.navigateTo({
            url: '/pages/search/index'
        })
    }

    render() {
        return (
            <View className='search-container'>
                <AtTabs
                    current={this.state.current}
                    scroll
                    tabList={[
                        { title: '标签页1' },
                        { title: '标签页2' },
                        { title: '标签页3' },
                        { title: '标签页4' },
                        { title: '标签页5' },
                        { title: '标签页6' }
                    ]}
                    onClick={this.handleClick.bind(this)}>
                    <AtTabsPane current={this.state.current} index={0}>
                        <View style='font-size:18px;text-align:center;height:100px;'>标签页一的内容</View>
                    </AtTabsPane>
                    <AtTabsPane current={this.state.current} index={1}>
                        <View style='font-size:18px;text-align:center;height:100px;'>标签页二的内容</View>
                    </AtTabsPane>
                    <AtTabsPane current={this.state.current} index={2}>
                        <View style='font-size:18px;text-align:center;height:100px;'>标签页三的内容</View>
                    </AtTabsPane>
                    <AtTabsPane current={this.state.current} index={3}>
                        <View style='font-size:18px;text-align:center;height:100px;'>标签页四的内容</View>
                    </AtTabsPane>
                    <AtTabsPane current={this.state.current} index={4}>
                        <View style='font-size:18px;text-align:center;height:100px;'>标签页五的内容</View>
                    </AtTabsPane>
                    <AtTabsPane current={this.state.current} index={5}>
                        <View style='font-size:18px;text-align:center;height:100px;'>标签页六的内容</View>
                    </AtTabsPane>
                </AtTabs>
                <View className='search-btn' onClick={this.handleNaviToSearch}>
                    <Image style='height:50%;width:50%' src={SearchIcon} />
                </View>
            </View>
        )
    }
}

export default Index;
