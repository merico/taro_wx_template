import Taro, { Component } from '@tarojs/taro';
import { Swiper, SwiperItem, ScrollView } from '@tarojs/components';

import './index.scss';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            goods: ['A', 'B', 'C', 'D', 'E', 'A', 'B', 'C', 'D', 'E']
        }
    }

    config = {
        navigationBarTitleText: '墨菲设计'
    };

    naviToBuyPage = (ev) => {
        Taro.navigateTo({
            url: `/pages/buy/index?id=${ev.currentTarget.dataset.id}`,
        })
    };

    // 开启当前页分享病设置分享内容
    onShareAppMessage() {
        return {
            title: '基于Taro框架开发的时装衣橱',
            path: '/pages/home/index',
        }
    };

    // 小程序上拉加载
    onReachBottom = () => {
        this.loadMoreGoods();
    }

    loadMoreGoods = () => {
        let currentGoods = this.state.goods.slice().concat(['A', 'B', 'C', 'D']);
        this.setState({
            goods: currentGoods
        });
    }

    render() {
        return (
            <View className='home-container'>
                <Swiper
                    className='home-swiper'
                    indicatorColor='#999'
                    indicatorActiveColor='#333'
                    circular
                    indicatorDots
                    autoplay>
                    <SwiperItem className='swiper-item'>
                        <View className='swiper-item_content'>1</View>
                    </SwiperItem>
                    <SwiperItem className='swiper-item'>
                        <View className='swiper-item_content'>2</View>
                    </SwiperItem>
                    <SwiperItem className='swiper-item'>
                        <View className='swiper-item_content'>3</View>
                    </SwiperItem>
                </Swiper>

                <View className='home-recomendation'>
                    <View className='home-recomendation_title'>
                        <Text> - 卖家推荐 - </Text>
                    </View>
                    <View className='home-recomendation_list'>
                        {this.state.goods.map((good, index) => (
                            <View
                                key={index}
                                data-id={good}
                                className='home-recomendation_list_item'
                                onClick={this.naviToBuyPage}>
                                {good}
                            </View>
                        ), this)
                        }
                    </View>
                </View>
            </View>
        )
    }
}

export default Index;
