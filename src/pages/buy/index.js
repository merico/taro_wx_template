import Taro, { Component } from '@tarojs/taro';

class Buy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            goodsId: null
        }
    }

    config = {
        navigationBarTitleText: '商品详情',
    };

    componentWillMount() {
        this.setState({
            goodsId: this.$router.params.id,
        })
    }

    render() {
        return (
            <View>
                <Text>{this.state.goodsId}</Text>
            </View>
        )
    }
}

export default Buy;