import { Component } from '@tarojs/taro';
import CustomRadio from '../Radio';
import { AtInputNumber, AtSwipeAction } from 'taro-ui'

import './index.scss';

export default class Goods extends Component {
    constructor(props) {
        super(props);
        this.index = this.props.index;
        this.state = {
            selected: this.props.selected || false,
            purchaseNumber: this.props.number || 1
        };
    }

    componentWillReceiveProps(newProps) {
        if (this.state.selected != newProps.selected) {
            this.setState({
                selected: newProps.selected
            })
        }
    }

    handleChangePurchaseNumber = (value) => {
        this.setState({
            purchaseNumber: value
        }, () => {
            this.props.onHandleAddOneOrMinusOne(this.index, this.state.purchaseNumber);
        })
    }

    handleCheckGoodsPicture = (ev) => {
        let imageSrc = ev.target.dataset.src;
        this.props.onHandleShowCurtain(imageSrc);
    }

    handleSwipeAction = (ev) => {
        console.log(ev);
    }

    handleSelectOrNotSelectGoods = (key, flag) => {
        this.props.onChange(key, flag);
    }

    render() {
        return (
            <AtSwipeAction autoClose onClick={this.handleSwipeAction} options={[
                {
                    text: '移除',
                    style: {
                        backgroundColor: '#FF4949'
                    }
                }
            ]}>
                <View className='goods-container'>
                    <View className='goods-select'>
                        <CustomRadio
                            index={this.index}
                            selected={this.state.selected}
                            onChange={this.handleSelectOrNotSelectGoods}
                        />
                    </View>
                    <View className='goods-pic'>
                        <Image
                            style='width: 100%;height: 100%;;background: #fff;'
                            onClick={this.handleCheckGoodsPicture}
                            src={this.props.imageSrc}
                            data-src={this.props.imageSrc}
                        />
                    </View>
                    <View className='goods-info'>
                        <View className='goods-info-name'>
                            <Text>{this.props.name}</Text>
                        </View>
                        <View className='goods-info-size-price'>
                            <View className='goods-info-size'>
                                <Text>尺码：{this.props.size}</Text>
                            </View>
                            <View className='goods-info-price'>
                                <Text>单价：¥{this.props.price}</Text>
                            </View>
                        </View>
                        <View className='goods-info-number'>
                            <AtInputNumber min={1} step={1} width={150}
                                value={this.state.purchaseNumber}
                                onChange={this.handleChangePurchaseNumber}
                            />
                        </View>
                    </View>
                </View>
            </AtSwipeAction>
        )
    }
}