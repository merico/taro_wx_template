import { Component } from '@tarojs/taro';
import { Image, View } from '@tarojs/components';

import UnSelected from '../../images/components/radio_unselected.png';
import Selected from '../../images/components/radio_selected.png';

export default class Index extends Component {
    constructor(props) {
        super(props);
        this.width = this.props.width || 30;
        this.height = this.props.height || 30;
        this.index = this.props.index;
        this.state = {
            selected: this.props.selected || false
        }
    }

    componentWillReceiveProps(newProps) {
        if (this.state.selected != newProps.selected) {
            this.setState({
                selected: newProps.selected
            })
        }
    }

    onChange = () => {
        this.setState({
            selected: !this.state.selected
        }, () => {
            this.props.onChange(this.index, this.state.selected);
        })
    }

    render() {
        return (
            <View style={{ height: '30px', width: '30px' }}>
                {
                    this.state.selected ?
                        (<Image src={Selected} style={`width: ${this.width}px;height: ${this.height}px`} onClick={this.onChange} />) :
                        (<Image src={UnSelected} style={`width: ${this.width}px;height: ${this.height}px`} onClick={this.onChange} />)
                }
            </View>
        )
    }
}